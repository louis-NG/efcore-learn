﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EFCORE_LEARN.Entities;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Data.SqlClient;

namespace EFCORE_LEARN.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeachersController : ControllerBase
    {
        private  SchoolContext _context;

        public TeachersController(SchoolContext context)
        {
            _context = context;
        }


        // GET: api/Teachers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Teacher>>> GetTeachers(string Name)
        {
            var param = new SqlParameter("@Name",Name);
            //return await _context.Teachers.FromSqlRaw("GetTeachers {0}",Name).ToListAsync();
            return await _context.Teachers.FromSqlRaw("GetTeachers @Name", param).ToListAsync();
        }

        // GET: api/Teachers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Teacher>> GetTeacher(int id)
        {
            var teacher = await _context.Teachers.FindAsync(id);
            DisplayStates(_context.ChangeTracker.Entries());
            if (teacher == null)
            {
                return NotFound();
            }
            return teacher;
        }

        // PUT: api/Teachers/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTeacher()
        {
            var temp = _context.Teachers.Find(1);
            if (temp == null)
            {
                return BadRequest();
            }
            temp.TeacherName = "aaaaa";
            _context.Entry(temp).State = EntityState.Modified;
            DisplayStates(_context.ChangeTracker.Entries());
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Teachers
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Teacher>> PostTeacher()
        {
            using (var context = new SchoolContext())
            {
               // context.Add(new Teacher() { TeacherName = "Louis" });
                var tc = new Teacher() { TeacherName = "Emma" };
                context.Attach(tc).State = EntityState.Added;

                foreach (var entry in context.ChangeTracker.Entries())
                {
                    Console.WriteLine($"=====================Entity: {entry.Entity.GetType().Name}, State: {entry.State.ToString()}=========================");
                }
            }
            //var teacher = new Teacher() {TeacherName = "Mark" };
            //_context.Teachers.Add(teacher);
            //DisplayStates(_context.ChangeTracker.Entries());

            await _context.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/Teachers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Teacher>> DeleteTeacher(int id)
        {
            var teacher = await _context.Teachers.FindAsync(1);
            if (teacher == null)
            {
                return NotFound();
            }

            _context.Teachers.Remove(teacher);
            DisplayStates(_context.ChangeTracker.Entries());
            await _context.SaveChangesAsync();

            return teacher;
        }

        private bool TeacherExists(int id)
        {
            return _context.Teachers.Any(e => e.TeacherId == id);
        }
        private static void DisplayStates(IEnumerable<EntityEntry> entries)
        {
            foreach (var entry in entries)
            {
                Console.WriteLine($"Entity: {entry.Entity.GetType().Name}, State: {entry.State.ToString()}");
            }
        }
       
    }
}
