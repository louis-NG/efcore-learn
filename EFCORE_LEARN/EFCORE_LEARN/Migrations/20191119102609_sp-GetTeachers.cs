﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCORE_LEARN.Migrations
{
    public partial class spGetTeachers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"CREATE PROCEDURE [dbo].[GetTeachers]
                    @Name varchar(50)
                AS
                BEGIN
                    SET NOCOUNT ON;
                    select * from Teachers where TeacherName like @Name +'%'
                END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
